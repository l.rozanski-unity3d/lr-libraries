﻿using System.Collections.Generic;
using System.Web.Management;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;

namespace LR.Core.Editor.Input {
    public class PlayerInputCombiner : EditorWindow {

        private readonly List<InputActionAsset> inputActions = new();

        private void CreateGUI() {
            inputActions.Clear();

            var uxml = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Packages/com.lr.core/Editor/Input/PlayerInputCombiner.uxml").CloneTree();
            var stylesheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Packages/com.lr.core/Editor/Input/PlayerInputCombiner.uss");

            var root = uxml.Q("Root");
            rootVisualElement.Add(root);
            rootVisualElement.styleSheets.Add(stylesheet);

            var list = root.Q<ScrollView>("List");
            list.Add(CreateListItem());

            root.Q<Button>("CombineButton").clicked += Combine;
        }

        private ObjectField CreateListItem() {
            var list = rootVisualElement.Q<ScrollView>("List");

            var objectField = new ObjectField("Input Actions") {
                objectType = typeof(InputActionAsset),
                allowSceneObjects = false
            };

            objectField.RegisterCallback<ChangeEvent<InputActionAsset>>(evt => {
                Debug.Log($"Event value: {evt.newValue}");
                var value = evt.newValue;

                if (value != null) {
                    inputActions.Add(value);
                    list.Add(CreateListItem());
                } else {
                    inputActions.Remove(value);
                    list.Query<ObjectField>().Last().RemoveFromHierarchy();
                }
            });

            return objectField;
        }

        private void Combine() {
            foreach (var asset in inputActions) {
                Debug.Log(asset.name);
            }
        }

        #region Create window

        [MenuItem("Tools/LR/Combine Input Maps")]
        public static void OpenWindow() {
            var rect = GUIHelper
                .GetEditorWindowRect()
                .AlignCenter(400, 400);

            GetWindowWithRect<PlayerInputCombiner>(rect, false, "Combine Input Maps", true);
        }

        #endregion

    }
}
