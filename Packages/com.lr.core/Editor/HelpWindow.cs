﻿using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using Sirenix.Utilities.Editor.Expressions;
using UnityEditor;

namespace LR.Core.Editor {
    public class HelpWindow : OdinMenuEditorWindow {

        /// <summary>
        /// Used for syntax highlighting
        /// </summary>
        /// <see cref="Sirenix.OdinInspector.Editor.Examples.SyntaxHighlighter"/>
        private static readonly Tokenizer Tokenizer = new();

        [MenuItem("Tools/LR/Show Help")]
        public static void CreateEditor() {
            var rect = GUIHelper
                .GetEditorWindowRect()
                .AlignCenter(800, 800);

            var window = GetWindow<HelpWindow>();
            window.titleContent.text = "LR Libraries Help Window";
            window.position = rect;
        }

        protected override OdinMenuTree BuildMenuTree() {
            var editorTree = new OdinMenuTree(false, OdinMenuStyle.TreeViewStyle);
            var coreMenuItem = new OdinMenuItem(editorTree, "Core", null) {
                DefaultToggledState = true,
                ChildMenuItems = {
                    new OdinMenuItem(editorTree, "Extensions", null),
                    new OdinMenuItem(editorTree, "Utils", new UtilsPage())
                }
            };

            editorTree.MenuItems.Add(coreMenuItem);
            return editorTree;
        }

    }
}
