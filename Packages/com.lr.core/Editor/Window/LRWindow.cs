﻿using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace LR.Core.Editor.Window {
    // ReSharper disable once InconsistentNaming
    public class LRWindow : OdinEditorWindow {

        private WindowData data;

        private readonly Dictionary<string, VisualElement> contentTabs = new();

        private void CreateGUI() {
            contentTabs.Clear();
            data = CreateInstance<WindowData>();

            var uxml = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Packages/com.lr.core/Editor/Window/Window.uxml");
            var stylesheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Packages/com.lr.core/Editor/Window/Window.uss");

            rootVisualElement.Add(uxml.CloneTree().Q("Root"));
            rootVisualElement.styleSheets.Add(stylesheet);

            rootVisualElement.Q<Button>("CoreMenuItem").clicked += () => data.selectedMenuItem = "Core";
            rootVisualElement.Q<Button>("BuildingSubMenuItem").clicked += () => data.selectedMenuItem = "Building";

            rootVisualElement.Q<Label>("Title").Bind(new SerializedObject(data));
            var contentContainer = rootVisualElement.Q<VisualElement>("Content");

            AddTabContent(
                contentContainer,
                "CoreTab",
                "Packages/com.lr.core/Editor/Window/CoreTab.uxml",
                "Packages/com.lr.core/Editor/Window/CoreTab.uss"
            );

#if LR_GRIDS_BUILDING
            AddTabContent(
                contentContainer,
                "BuildingTab",
                "Packages/com.lr.grids.building/Editor/Window/TabContent.uxml",
                "Packages/com.lr.grids.building/Editor/Window/TabContent.uss",
                "Packages/com.lr.grids.building/Prefabs/Components/style.uss"
            );
#endif

            rootVisualElement.Q<Label>("Title").RegisterCallback<ChangeEvent<string>>(SwitchTab);
        }

        private void AddTabContent(VisualElement parent, string tabName, string uxmlPath, params string[] ussPaths) {
            var uxml = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(uxmlPath);
            var styleSheets = ussPaths
                .Select(path => AssetDatabase.LoadAssetAtPath<StyleSheet>(path))
                .ToList();

            if (uxml == null) {
                Debug.LogError($"Cannot find UXML for tab {tabName}");
                return;
            }
            var tabContent = uxml.CloneTree().Q(tabName);

            foreach (var styleSheet in styleSheets) {
                tabContent.styleSheets.Add(styleSheet);
            }

            tabContent.style.display = DisplayStyle.None;
            contentTabs.Add(tabName, tabContent);
            parent.Add(tabContent);
        }

        private void SwitchTab(ChangeEvent<string> changeEvent) {
            var currentTab = contentTabs[$"{changeEvent.previousValue}Tab"];
            currentTab.style.display = DisplayStyle.None;

            var newTab = contentTabs[$"{changeEvent.newValue}Tab"];
            newTab.style.display = DisplayStyle.Flex;
        }

        #region Create window

        [MenuItem("Tools/LR/Open Window")]
        public static void OpenWindow() {
            var rect = GUIHelper
                .GetEditorWindowRect()
                .AlignCenter(800, 800);

            GetWindowWithRect<LRWindow>(rect, false, "LR Libraries", true);
        }

        #endregion

        private class WindowData : ScriptableObject {

            public string selectedMenuItem = "Core";

        }

    }
}
