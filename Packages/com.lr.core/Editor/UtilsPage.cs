﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace LR.Core.Editor {
    [Serializable]
    public class UtilsPage {

        [TextArea, ReadOnly]
        public string Example = "This is a test\n\ntest";

        [Title("This is a test")]
        [BoxGroup("Test")]
        public float x = 1f;

    }
}
