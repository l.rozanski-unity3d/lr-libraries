﻿using System;
using System.Linq;
using UnityEditor;

namespace LR.Core.Editor {
    internal static class DefineSymbolAppender {

        private const string DEFINE_SYMBOL = "LR_CORE";

        [InitializeOnLoadMethod]
        private static void RegisterDefineSymbol() {
            var currentTarget = EditorUserBuildSettings.selectedBuildTargetGroup;

            if (currentTarget == BuildTargetGroup.Unknown) {
                return;
            }
            var definesString = PlayerSettings.GetScriptingDefineSymbolsForGroup(currentTarget).Trim();
            var defines = definesString.Split(';').ToList();

            var changed = false;

            if (!defines.Contains(DEFINE_SYMBOL)) {
                if (!definesString.EndsWith(";", StringComparison.InvariantCulture)) {
                    definesString += ";";
                }

                definesString += DEFINE_SYMBOL;
                changed = true;
            }

            if (changed) {
                PlayerSettings.SetScriptingDefineSymbolsForGroup(currentTarget, definesString);
            }
        }

    }
}
