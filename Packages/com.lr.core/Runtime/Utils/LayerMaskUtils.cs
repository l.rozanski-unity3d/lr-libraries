﻿using System.Collections.Generic;
using UnityEngine;

namespace LR.Core.Utils {
    public static class LayerMaskUtils {

        private const int MaxLayerIndex = 31;

        public static List<string> GetAllLayerNames(bool onlyUserDefined = false) {
            var layerNames = new List<string>();
            var startIndex = onlyUserDefined ? 8 : 0;

            // user defined layers start with layer 8 and unity supports 31 layers
            for (var i = startIndex; i <= MaxLayerIndex; i++) {
                var layer = LayerMask.LayerToName(i);

                if (layer.Length > 0) {
                    //only add the layer if it has been named
                    layerNames.Add(layer);
                }
            }
            return layerNames;
        }

    }
}
