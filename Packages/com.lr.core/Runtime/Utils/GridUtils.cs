﻿using System.Diagnostics.CodeAnalysis;
using UnityEngine;

namespace LR.Core.Utils {
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public static class GridUtils {

        #region Snap to grid

        public static Vector3 SnapToGrid(Vector3 position, Vector2 gridSize, bool round) {
            return new Vector3(
                round ? Mathf.RoundToInt(position.x / gridSize.x) * gridSize.x : Mathf.FloorToInt(position.x / gridSize.x) * gridSize.x,
                round ? Mathf.RoundToInt(position.y / gridSize.y) * gridSize.y : Mathf.FloorToInt(position.y / gridSize.y) * gridSize.y,
                position.z
            );
        }

        #endregion

        #region 2D Coords to index

        public static int Coords2DToIndex(int x, int y, int width) => x + y * width;

        #endregion

        #region Index to 2D coords

        public static Vector3Int IndexTo2DCoords(int index, int width) => new(index % width, index / width);

        #endregion

    }
}
