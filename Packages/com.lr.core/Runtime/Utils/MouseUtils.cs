﻿using System;
using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.InputSystem;

namespace LR.Core.Utils {
    [SuppressMessage("ReSharper", "UnusedType.Global")]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public static class MouseUtils {

        public const int LeftButton = 0;
        public const int RightButton = 1;
        public const int MiddleButton = 2;

        public static Vector2 WorldPosition => GetWorldPosition();

        public static Vector2 GetWorldPosition([CanBeNull] Camera camera = null) {
            var screenPosition = Mouse.current.position.ReadValue();

            if (Camera.main == null) {
                throw new InvalidOperationException("Camera.main has been requested, but was null");
            }
            return camera != null
                ? camera.ScreenToWorldPoint(screenPosition)
                : Camera.main.ScreenToWorldPoint(screenPosition);
        }

    }
}
