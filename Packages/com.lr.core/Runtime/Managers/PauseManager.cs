﻿using UnityEngine.InputSystem;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace LR.Core.Managers {
    public class PauseManager : SingletonMonoBehaviour<PauseManager> {

#if UNITY_EDITOR

        private void Update() {
            if (Keyboard.current.pKey.wasPressedThisFrame) {
                Pause();
            }
        }

        private static void Pause() => EditorApplication.isPaused = true;

#endif

    }
}
