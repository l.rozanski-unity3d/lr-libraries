﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using UnityEditor;
using UnityEngine;

namespace LR.Core.Extensions {
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public static class TransformExtensions {

        public static List<Transform> GetChildren(this Transform transform) {
            var children = new List<Transform>();

            for (var i = 0; i < transform.childCount; i++) {
                children.Add(transform.GetChild(i));
            }
            return children;
        }

        public static void DestroyChildren(this Transform transform, bool destroyInEditor = false) {
            var children = transform.GetChildren();

            for (var i = children.Count - 1; i >= 0; i--) {
#if UNITY_EDITOR
                if (EditorApplication.isPlaying || destroyInEditor) {
                    Object.DestroyImmediate(children[i].gameObject);
                }
#else
                Object.Destroy(children[i].gameObject);
#endif
            }
        }
    }
}
