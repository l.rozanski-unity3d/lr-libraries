﻿#define LR_CORE
using UnityEngine;

namespace LR.Core {
    public class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour {

        // ReSharper disable once MemberCanBePrivate.Global
        // ReSharper disable once UnusedAutoPropertyAccessor.Global
        public static T Instance { get; private set; }

        private void Awake() {
            Instance = this as T;
        }
    }
}
