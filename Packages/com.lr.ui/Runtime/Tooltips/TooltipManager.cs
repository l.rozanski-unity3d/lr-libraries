﻿#define LR_UI

using System;
using Cysharp.Threading.Tasks;
using LR.Core;
using LR.Core.Utils;
using Sirenix.OdinInspector;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace LR.UI.Tooltips {
    public class TooltipManager : SingletonMonoBehaviour<TooltipManager> {

        [SerializeField, ChildGameObjectsOnly, Required]
        private RectTransform uiTooltip;

        [SerializeField, ChildGameObjectsOnly, Required]
        private TextMeshProUGUI uiText;

        [SerializeField]
        private LayerMask tooltipLayerMask;

        [SerializeField]
        private Canvas canvas;

        [SerializeField]
        private Vector2 offset = new(0.5f, 0f);

        [SerializeField, PropertyRange(0f, 20f)]
        private float padding = 10f;

        [SerializeField, PropertyRange(0f, 1000f)]
        private float updateInterval = 100f;

        [SerializeField, PropertyRange(0f, 1000f)]
        private float maxWidth;

        private void Start() {
            uiTooltip.gameObject.SetActive(false);
            ShowTooltip().Forget();
        }

        private async UniTaskVoid ShowTooltip() {
            while (true) {
                await UniTask.Delay(TimeSpan.FromMilliseconds(updateInterval), true);
                await UniTask.WaitForFixedUpdate();

                var mousePosition = MouseUtils.WorldPosition;
                var tooltipCollider = Physics2D.OverlapPoint(mousePosition, tooltipLayerMask);

                if (!tooltipCollider || !tooltipCollider.TryGetComponent(out Tooltip tooltip)) {
                    uiTooltip.gameObject.SetActive(false);
                    continue;
                }
                uiTooltip.GetComponentInChildren<TextMeshProUGUI>().text = tooltip.Message;
                uiTooltip.gameObject.SetActive(true);
                UpdatePositionAndSize();
            }
            // ReSharper disable once FunctionNeverReturns
        }

        private void LateUpdate() {
            if (!uiTooltip.gameObject.activeSelf) {
                return;
            }
            UpdatePositionAndSize();
        }

        private void UpdatePositionAndSize() {
            var tooltipPosition = MouseUtils.WorldPosition + offset;
            var width = Mathf.Min(uiText.preferredWidth, maxWidth);

            uiTooltip.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width + padding * 2f);
            uiTooltip.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, uiText.preferredHeight + padding * 2f);
            uiTooltip.SetPositionAndRotation(tooltipPosition, Quaternion.identity);

            var textRectTransform = uiText.rectTransform;
            textRectTransform.sizeDelta = new Vector2(width, uiText.preferredHeight);
        }

#if UNITY_EDITOR

        #region Editor

        [MenuItem("Tools/LR/UI/Create Tooltip Manager")]
        public static void CreateTooltipManager() {
            const string name = "Tooltip Manager";

            if (GameObject.Find(name)) {
                Debug.Log("Tooltip Manager already exists, aborting");
                return;
            }
            var manager = new GameObject(name).AddComponent<TooltipManager>();

            CreateCanvas(manager);
            Selection.activeGameObject = manager.gameObject;
        }

        private static void CreateCanvas(TooltipManager manager) {
            var canvas = new GameObject("Canvas").AddComponent<Canvas>();
            canvas.transform.SetParent(manager.transform, false);
            canvas.worldCamera = Camera.main;
            canvas.renderMode = RenderMode.ScreenSpaceCamera;

            manager.canvas = canvas;
        }

        [Button, ShowIf("@" + nameof(uiTooltip) + " == null")]
        public void CreateTooltip() {
            if (canvas == null) {
                CreateCanvas(this);
            }
            var tooltip = new GameObject("Tooltip").AddComponent<Tooltip>();
            tooltip.transform.SetParent(canvas.transform, false);
            tooltip.Message = "This is a sample tooltip";

            var panel = tooltip.gameObject.AddComponent<Image>();
            panel.color = Color.black;

            var rectTransform = panel.rectTransform;
            rectTransform.SetParent(tooltip.transform, false);
            rectTransform.pivot = new Vector2(0f, 1f);

            rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0f, 300f);
            rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0f, 48f);

            var text = new GameObject("Text").AddComponent<TextMeshProUGUI>();
            text.text = tooltip.Message;

            var textRectTransform = text.rectTransform;
            textRectTransform.SetParent(rectTransform, false);
            textRectTransform.anchorMin = Vector2.one / 2f;
            textRectTransform.anchorMax = Vector2.one / 2f;
            textRectTransform.pivot = Vector2.one / 2f;

            textRectTransform.sizeDelta = new Vector2(300f - padding * 2f, 48f - padding * 2f);
            text.horizontalAlignment = HorizontalAlignmentOptions.Center;
            text.verticalAlignment = VerticalAlignmentOptions.Middle;
            text.fontSize = 18f;

            uiTooltip = tooltip.GetComponent<RectTransform>();
            uiText = text;

            tooltip.gameObject.SetActive(false);
            Selection.activeGameObject = tooltip.gameObject;
        }

        #endregion

#endif

    }
}
