﻿using UnityEngine;

namespace LR.UI.Tooltips {
    public class Tooltip : MonoBehaviour {

        [field: SerializeField, TextArea]
        public string Message { get; set; }

    }
}
