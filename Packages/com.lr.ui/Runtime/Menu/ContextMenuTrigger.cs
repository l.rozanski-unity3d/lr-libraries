﻿using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace LR.UI.Menu {
    public class ContextMenuTrigger : MonoBehaviour {

        [SerializeField, Required, ValueDropdown(nameof(GetAvailableContextMenus))]
        private string contextMenuName;

        [SerializeField, Required]
        private Component context;

        public string ContextMenuName => contextMenuName;
        public Component Context => context;

        private List<string> GetAvailableContextMenus() {
            return ContextMenuManager.ContextMenus == null
                ? new List<string>(0)
                : ContextMenuManager.ContextMenus.Select(entry => entry.Key).ToList();
        }

#if UNITY_EDITOR

        #region Editor

        [MenuItem("Tools/LR/UI/Create Context Menu Manager")]
        public static void CreateContextMenuManager() {
            const string name = "Context Menu Manager";

            if (GameObject.Find(name)) {
                Debug.Log("Context Menu Manager already exists, aborting");
                return;
            }
            var manager = new GameObject(name).AddComponent<ContextMenuManager>();
            Selection.activeGameObject = manager.gameObject;
        }

        #endregion

#endif

    }
}
