﻿using System.Diagnostics.CodeAnalysis;
using UnityEngine;

namespace LR.UI.Menu {
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public interface IContextMenu {

        #region MonoBehaviour properties

        string name { get; set; }
        GameObject gameObject { get; }
        Transform transform { get; }

        #endregion

        void Init(Component context);
        
        void OnOpen();
        void OnClose();

    }
}
