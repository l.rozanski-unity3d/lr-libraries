﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace LR.UI.Menu {
    public abstract class AbstractContextMenu<T> : MonoBehaviour, IContextMenu where T : Component {

        [ShowInInspector, ReadOnly]
        protected T Context;

        public void Init(Component currentContext) => Context = currentContext as T;

        public virtual void OnOpen() { }
        public virtual void OnClose() { }

    }
}
