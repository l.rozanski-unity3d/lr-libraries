﻿#define LR_UI

using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using LR.Core;
using LR.Core.Utils;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UIElements;

namespace LR.UI.Menu {
    public class ContextMenuManager : SingletonMonoBehaviour<ContextMenuManager> {

        [SerializeField]
        private Vector2 offset = new(0.5f, 0f);

        [SerializeField]
        private LayerMask contextMenuLayerMask;

        [ShowInInspector, ReadOnly]
        public static readonly Dictionary<string, IContextMenu> ContextMenus = new();

        private IContextMenu currentContextMenu;

        private void Start() {
            UpdateContextMenus();
        }

        private void LateUpdate() {
            if (!Input.GetMouseButtonDown((int) MouseButton.RightMouse)) {
                return;
            }
            var targetCollider = Physics2D.OverlapPoint(MouseUtils.WorldPosition, contextMenuLayerMask);
            if (targetCollider == null || !targetCollider.TryGetComponent(out ContextMenuTrigger contextMenuTrigger)) {
                return;
            }
            if (!ContextMenus.ContainsKey(contextMenuTrigger.ContextMenuName)) {
                return;
            }
            ShowContextMenu(contextMenuTrigger);
        }

        private void ShowContextMenu(ContextMenuTrigger contextMenuTrigger) {
            CloseCurrentMenu();

            var contextMenu = ContextMenus[contextMenuTrigger.ContextMenuName];
            currentContextMenu = contextMenu;

            contextMenu.gameObject.SetActive(true);
            contextMenu.transform.position = MouseUtils.WorldPosition + offset;
            contextMenu.Init(contextMenuTrigger.Context);
            contextMenu.OnOpen();
        }

        [UsedImplicitly]
        public void CloseCurrentMenu() {
            if (currentContextMenu == null) {
                return;
            }
            currentContextMenu.OnClose();
            currentContextMenu.gameObject.SetActive(false);
        }

        [UsedImplicitly]
        public List<string> GetAvailableContextMenus() {
            return GetComponentsInChildren<IContextMenu>(true)
                .Select(contextMenu => contextMenu.name)
                .OrderBy(name => name)
                .ToList();
        }

        [Button]
        public void UpdateContextMenus() {
            ContextMenus.Clear();

            foreach (var contextMenu in GetComponentsInChildren<IContextMenu>(true)) {
                ContextMenus.Add(contextMenu.name, contextMenu);
                contextMenu.gameObject.SetActive(false);
            }
        }

    }
}
