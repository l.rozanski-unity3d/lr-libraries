﻿using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;

namespace LR.UI.Stats {
    public class HealthBar : MonoBehaviour {

        [SerializeField]
        private float hp;

        [SerializeField]
        private float maxHP;

        public float HP {
            get => hp;
            set {
                hp = Mathf.Clamp(value, 0f, MaxHP);
                UpdateHealthBar();
            }
        }

        public float MaxHP {
            get => maxHP;
            set {
                maxHP = Mathf.Max(HP, value);
                UpdateHealthBar();
            }
        }

        [field: SerializeField, PropertyRange(0f, 10f)]
        public float DamageTimeout { get; set; }

        [SerializeField]
        private UIDocument uiDocument;

        private VisualElement health;
        private VisualElement damage;
        private VisualElement damageQuick;

        private CancellationTokenSource disableToken;
        private float timeoutLeft;

        private void UpdateHealthBar() {
            health.style.width = new StyleLength(Length.Percent(hp / MaxHP * 100f));
            damageQuick.style.width = health.style.width;
            timeoutLeft = DamageTimeout;
        }

        private void OnEnable() {
            disableToken?.Dispose();
            disableToken = new CancellationTokenSource();
        }

        private void OnDisable() {
            disableToken.Cancel();
        }

        private void Start() {
            var healthBar = uiDocument.rootVisualElement.Q<VisualElement>("HealthBar");
            health = healthBar.Q<VisualElement>("Health");
            damage = healthBar.Q<VisualElement>("Damage");
            damageQuick = healthBar.Q<VisualElement>("DamageQuick");

            UpdateHealthBar();
            damage.style.width = health.style.width;

            ClearDamage().Forget();
        }

        private void Update() {
            if (timeoutLeft > 0f) {
                timeoutLeft = Mathf.Max(timeoutLeft - Time.deltaTime, 0f);
            }
        }

        private async UniTaskVoid ClearDamage() {
            while (!disableToken.IsCancellationRequested) {
                await UniTask.WaitUntilValueChanged(this, bar => bar.HP);
                await UniTask.WaitUntil(() => timeoutLeft <= 0f);

                damage.style.width = health.style.width;
            }
        }

        [Button, DisableInEditorMode]
        public void TakeDamage([MinMaxSlider(0f, 10f)] Vector2 damage) {
            HP -= Random.Range(damage.x, damage.y);
        }
    }
}
