# Installation

Go to Unity, open the `Package Manager`, select `Add package from git URL...` and enter:
```
ssh://git@gitlab.com/l.rozanski-unity3d/lr-libraries.git?path=/Packages/LR.FSM
```
