﻿using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;

namespace LR.FSM.Editor {
    // ReSharper disable once UnusedType.Global
    public class StateValueDrawer<T, E> : OdinValueDrawer<T> where T : State<E> where E : MonoBehaviour {

        protected override void DrawPropertyLayout(GUIContent label) {
            var state = ValueEntry.SmartValue;

            EditorGUILayout.BeginHorizontal();
            using (new EditorGUI.DisabledScope(true)) {
                SirenixEditorFields.UnityObjectField(state, typeof(T), true);
            }
            var active = state.StateMachine.CurrentState == state;
            GUI.backgroundColor = active ? Color.green : Color.white;

            using (new EditorGUI.DisabledScope(active)) {
                if (GUILayout.Button(active ? "Active" : "Activate", GUILayout.Width(128f))) {
                    state.StateMachine.ActivateState(state.name);
                }
            }
            GUI.backgroundColor = Color.white;
            EditorGUILayout.EndHorizontal();
        }
    }
}
