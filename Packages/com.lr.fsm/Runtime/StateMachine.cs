﻿#define LR_FSM
using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

namespace LR.FSM {
    [Serializable]
    public class StateMachine<T> where T : MonoBehaviour {

        // ReSharper disable once UnusedMember.Local
        // ReSharper disable once StaticMemberInGenericType
        private static readonly Color DarkGreen = new Color(0f, 0.25f, 0f);

        [ShowInInspector, ReadOnly]
        private T entityComponent;

        [ShowInInspector]
        [ListDrawerSettings(HideAddButton = true, HideRemoveButton = true, DraggableItems = false, Expanded = true, ElementColor = "@$value[$index] == CurrentState ? DarkGreen : $defaultColor")]
        private State<T>[] states;

        private Dictionary<string, State<T>> nameToState;

        public State<T> CurrentState { get; private set; }

        public void Init(T entityComponent) {
            this.entityComponent = entityComponent;

            states = entityComponent.GetComponentsInChildren<State<T>>();
            nameToState = states.ToDictionary(state => state.name);

            foreach (var state in states) {
                state.Init(this);
            }
        }

#if UNITY_EDITOR

        private void DrawStateItem(int index) {
            if (GUILayout.Button("Activate")) {
                ActivateState(index);
            }
        }

#else
private void DrawStateItem(int index) {}
#endif

        public void ActivateState(int index) {
            if (CurrentState != null) {
                CurrentState.OnExit(entityComponent);
            }
            CurrentState = states[index];
            CurrentState.OnEnter(entityComponent);
        }

        public void ActivateState(string name) {
            if (CurrentState != null) {
                CurrentState.OnExit(entityComponent);
            }
            CurrentState = nameToState[name];
            CurrentState.OnEnter(entityComponent);
        }

        public void UpdateState() {
            if (CurrentState != null) {
                CurrentState.OnUpdate(entityComponent);
            }
        }
    }
}
