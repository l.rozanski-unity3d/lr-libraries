﻿using UnityEngine;

namespace LR.FSM {

    public abstract class State<T> : MonoBehaviour where T : MonoBehaviour {

        public StateMachine<T> StateMachine { get; private set; }

        public void Init(StateMachine<T> stateMachine) => StateMachine = stateMachine;

        public abstract void OnEnter(T instance);
        public abstract void OnUpdate(T instance);
        public abstract void OnExit(T instance);
    }
}
