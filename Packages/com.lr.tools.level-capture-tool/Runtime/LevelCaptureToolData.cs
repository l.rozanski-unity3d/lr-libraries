﻿using JetBrains.Annotations;
using UnityEngine;

namespace LR.Tools.LevelCaptureTool {
    public class LevelCaptureToolData : ScriptableObject {

        [field: SerializeField]
        public RenderTexture RenderTexture { get; set; }

        [SerializeField]
        public int pixelsPerUnit = 16;

        [SerializeField]
        public LayerMask layerMask;

        [field: SerializeField]
        public Vector2 Position { get; set; }

        [field: SerializeField]
        public Vector2 Size { get; set; }

        [SerializeField]
        public float margin;

        [field: SerializeField]
        public float ZOffset { get; [UsedImplicitly] set; } = -10f;

        [SerializeField]
        public bool saveLayersSeparately;

        [field: SerializeField]
        public Bounds Bounds { get; set; }

        public bool Modified { get; set; }

        [field: SerializeField]
        public Camera Camera { get; set; }

        public Rect ScreenRect => new(new Vector2(Position.x + Bounds.center.x, Position.y + Bounds.center.y), Bounds.size);

    }
}
