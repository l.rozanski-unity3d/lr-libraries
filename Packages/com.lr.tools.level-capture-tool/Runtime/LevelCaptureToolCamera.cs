﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace LR.Tools.LevelCaptureTool {
    [ExecuteAlways]
    public class LevelCaptureToolCamera : MonoBehaviour {

        [field: SerializeField]
        public LevelCaptureToolData Data { get; set; }

        public void MarkDirty() => Data.Modified = true;

        private void OnDisable() => ClearTexture();

        private void Start() {
            Data.Camera = GetComponent<Camera>();
            Data.Bounds = new Bounds(-Data.Size / 2f, Data.Size);
        }

        private void Update() {
            if (!Data.Modified) {
                return;
            }
            UpdateCamera();

#if UNITY_EDITOR
            EditorUtility.SetDirty(gameObject);
#endif
        }

        public void UpdateCamera() {
            ClearTexture();
            UpdateZPosition();

            var maxSize = Mathf.Max(Data.Bounds.size.x + Data.margin, Data.Bounds.size.y + Data.margin) * Data.pixelsPerUnit;
            var textureSize = CeilPower2((int) maxSize);
            Data.RenderTexture = new RenderTexture(textureSize, textureSize, 0, RenderTextureFormat.Default, 0);

            Data.Camera = GetComponent<Camera>();
            Data.Camera.aspect = 1f;
            Data.Camera.orthographicSize = textureSize / (float) Data.pixelsPerUnit / 2f;
            Data.Camera.targetTexture = Data.RenderTexture;
            Data.Camera.cullingMask = Data.layerMask;
        }

        public void ClearTexture() {
            if (Data.RenderTexture == null) {
                return;
            }
            RenderTexture.active = null;
            Data.RenderTexture.Release();
        }

        private void UpdateZPosition() {
            transform.position = new Vector3(
                transform.position.x,
                transform.position.y,
                Data.ZOffset
            );
        }

        /// <remarks>Taken from https://stackoverflow.com/a/54065600</remarks>
        private static int CeilPower2(int x) {
            if (x < 2) {
                return 1;
            }
            return (int) Math.Pow(2, (int) Math.Log(x - 1, 2) + 1);
        }

        private void OnValidate() {
            if (Data == null) {
                return;
            }
            if (Data.Camera == null) {
                Data.Camera = GetComponent<Camera>();
            }
            Data.Camera.orthographic = true;
            Data.Camera.aspect = 1f;
            Data.Size = new Vector2(
                Mathf.Max(Data.Size.x, 0f),
                Mathf.Max(Data.Size.y, 0f)
            );
            Data.pixelsPerUnit = Mathf.Max(Data.pixelsPerUnit, 1);
            Data.margin = Mathf.Max(Data.margin, 0f);
            UpdateZPosition();
        }

    }
}
