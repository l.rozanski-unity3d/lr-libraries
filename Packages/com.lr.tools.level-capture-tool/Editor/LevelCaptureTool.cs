﻿using System;
using System.Collections.Generic;
using System.IO;
using Cysharp.Threading.Tasks;
using UnityEditor;
using UnityEditor.EditorTools;
using UnityEditor.ShortcutManagement;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using Progress = UnityEditor.Progress;

namespace LR.Tools.LevelCaptureTool.Editor {
    [DefaultExecutionOrder(-1)]
    [EditorTool("Level Capture Tool")]
    public class LevelCaptureTool : EditorTool {

        public static LevelCaptureTool Instance { get; private set; }

        private static readonly HashSet<EventType> AcceptedEventTypes = new() {
            EventType.MouseDown,
            EventType.MouseDrag,
            EventType.MouseUp,
            EventType.KeyDown
        };

        private static readonly Color SelectionBgColor = new(0.07f, 0f, 0f, 0.4f);

        private GUIStyle actionBoxStyle;
        private GUIStyle toggleStyle;

        private bool selectionMode;
        private Vector2 start;
        private Vector2 end;

        private LayerMask captureLayers;

        private GUIContent iconContent;
        private LevelCaptureToolCamera toolCamera;

        public LevelCaptureToolData Data { get; set; }

        private void Awake() {
            var icon = AssetDatabase.LoadAssetAtPath<Texture2D>("Packages/com.lr.tools.level-capture-tool/Icons/tool-icon.png");
            iconContent = new GUIContent("Level Capture Tool", icon, "Level Capture Tool");

            Data = CreateInstance<LevelCaptureToolData>();
            Instance = this;
        }

        public override GUIContent toolbarIcon => iconContent;

        public override void OnActivated() {
            Instance = this;
            CreateCamera();
        }

        public override void OnWillBeDeactivated() => DestroyCamera();

        private void CreateCamera() {
            if (toolCamera != null) {
                return;
            }
            var existingCamera = GameObject.Find("LevelCaptureToolCamera");
            if (existingCamera != null) {
                DestroyImmediate(existingCamera);
            }
            var newToolCamera = new GameObject("LevelCaptureToolCamera") {
                tag = "EditorOnly",
                hideFlags = HideFlags.HideAndDontSave
            };
            toolCamera = newToolCamera.AddComponent<LevelCaptureToolCamera>();
            toolCamera.transform.position = Data.Position;
            toolCamera.Data = Data;

            var camera = newToolCamera.AddComponent<Camera>();
            camera.orthographic = true;

            Data.Camera = camera;
        }

        private void DestroyCamera() {
            if (toolCamera.gameObject == null) {
                return;
            }
            DestroyImmediate(toolCamera.gameObject);
        }

        public override void OnToolGUI(EditorWindow window) {
            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(GetHashCode(), FocusType.Passive));
            var bounds = FindBounds();

            DrawHandles(selectionMode ? bounds : Data.ScreenRect);

            var currentEvent = Event.current;
            var use = AcceptedEventTypes.Contains(currentEvent.type);
            var worldPos = HandleUtility.GUIPointToWorldRay(currentEvent.mousePosition).origin;

            if (use) {
                switch (currentEvent.type) {
                    case EventType.MouseDown:
                        selectionMode = true;
                        start = worldPos;
                        end = worldPos;
                        Data.Bounds = new Bounds();
                        currentEvent.Use();
                        break;
                    case EventType.MouseDrag:
                        end = worldPos;
                        currentEvent.Use();
                        break;
                    case EventType.MouseUp:
                        selectionMode = false;
                        UpdateCaptureRect(bounds);
                        FinishSelection();
                        currentEvent.Use();
                        break;
                    case EventType.KeyDown when currentEvent.keyCode == KeyCode.Escape:
                        selectionMode = false;
                        FinishSelection();
                        currentEvent.Use();
                        break;
                }
            }
        }

        private void UpdateCaptureRect(Rect bounds) {
            if (start == end) {
                return;
            }
            Data.Position = new Vector3(
                bounds.center.x,
                bounds.center.y,
                Data.ZOffset
            );
            Data.Size = new Vector2(
                Mathf.Abs(bounds.size.x),
                Mathf.Abs(bounds.size.y)
            );
            Data.Bounds = new Bounds(
                -Data.Size / 2f,
                Data.Size
            );

            toolCamera.gameObject.transform.position = Data.Position;
            toolCamera.MarkDirty();
        }

        private void FinishSelection() {
            start = end;
            toolCamera.MarkDirty();
            EditorUtility.SetDirty(toolCamera.gameObject);

            GUIUtility.hotControl = 0;
        }

        private Rect FindBounds() {
            var minX = Mathf.Min(start.x, end.x);
            var minY = Mathf.Min(start.y, end.y);
            var maxX = Mathf.Max(start.x, end.x);
            var maxY = Mathf.Max(start.y, end.y);

            return new Rect(minX, minY, maxX - minX, maxY - minY);
        }

        private void DrawHandles(Rect bounds) {
            if (bounds.size == Vector2.zero) {
                return;
            }
            Handles.color = Color.green;
            Handles.DrawWireCube(bounds.center, new Vector3(bounds.size.x + Data.margin, bounds.size.y + Data.margin));
            Handles.color = Color.white;
            Handles.DrawSolidRectangleWithOutline(bounds, SelectionBgColor, Color.red);

            HandleUtility.Repaint();
        }

        #region Old

        public void TrimBounds() {
            var colliders = Physics2D.OverlapAreaAll(Data.ScreenRect.min, Data.ScreenRect.max, Data.layerMask);
            if (colliders == null || colliders.Length == 0) {
                Debug.Log("No matching colliders found in the selected area");
                return;
            }

            var rect = FindColliderRect(colliders);
            if (!rect.HasValue) {
                return;
            }
            var rectCenter = rect.Value.center;

            var rectSize = rect.Value.size;
            if (Data.Bounds.size.x < rectSize.x || Data.Bounds.size.y < rectSize.y) {
                return;
            }
            Data.Position = new Vector3(rectCenter.x, rectCenter.y, Data.ZOffset);
            Data.Size = rectSize;
            Data.Bounds = new Bounds(-Data.Size / 2f, Data.Size);
            Data.Modified = false;

            toolCamera.transform.position = Data.Position;

            EditorUtility.SetDirty(toolCamera.gameObject);
            UpdateCamera();
        }

        private static Rect? FindColliderRect(Collider2D[] colliders) {
            var minX = float.MaxValue;
            var maxX = float.MinValue;
            var minY = float.MaxValue;

            var maxY = float.MinValue;

            // ReSharper disable once LocalVariableHidesMember
            foreach (var collider in colliders) {
                minX = Mathf.Min(minX, collider.bounds.min.x);
                maxX = Mathf.Max(maxX, collider.bounds.max.x);
                minY = Mathf.Min(minY, collider.bounds.min.y);
                maxY = Mathf.Max(maxY, collider.bounds.max.y);
            }
            if (minX >= float.MaxValue || maxX <= float.MinValue || minY >= float.MaxValue || maxY <= float.MinValue) {
                return null;
            }
            return new Rect(minX, minY, maxX - minX, maxY - minY);
        }

        public void UpdateCamera() => toolCamera.UpdateCamera();

        public async UniTaskVoid Capture() {
            var path = EditorUtility.SaveFilePanelInProject("Save captured level part", "", "png", "");

            if (string.IsNullOrEmpty(path)) {
                return;
            }
            var layerNames = new List<string>();

            for (var i = 0; i < 32; i++) {
                if (Data.layerMask != (Data.layerMask | (1 << i))) {
                    continue;
                }
                var layerName = LayerMask.LayerToName(i);
                if (string.IsNullOrEmpty(layerName)) {
                    continue;
                }
                layerNames.Add(LayerMask.LayerToName(i));
            }
            if (Data.saveLayersSeparately) {
                foreach (var layerName in layerNames) {
                    await SaveLayerMask(path, true, layerName);
                }
            } else {
                await SaveLayerMask(path, false, layerNames.ToArray());
            }
        }

        private async UniTask<object> SaveLayerMask(string path, bool appendLayerNames, params string[] layerNames) {
            var progressId = Progress.Start("Saving layers", $"{string.Join(",", layerNames)}");
            Debug.Log($"Capturing layers: {string.Join(", ", layerNames)}");
            UpdateCamera();

            var camera = Data.Camera;
            var cameraMask = camera.cullingMask;
            camera.cullingMask = 0;
            camera.cullingMask = LayerMask.GetMask(layerNames);
            camera.Render();
            camera.cullingMask = cameraMask;

            var active = RenderTexture.active;
            var renderTexture = Data.RenderTexture;
            RenderTexture.active = renderTexture;

            var texture = new Texture2D(renderTexture.width, renderTexture.height, renderTexture.graphicsFormat, 0, TextureCreationFlags.None);
            texture.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0, false);
            IncludeOnlyBounds(texture, Data.Bounds);

            texture.Apply();
            RenderTexture.active = active;
            await UniTask.Yield();

            var pngBytes = texture.EncodeToPNG();
            await UniTask.Yield();

            AssetDatabase.DisallowAutoRefresh();
            if (appendLayerNames) {
                var extensionIndex = path.LastIndexOf(".", StringComparison.Ordinal);
                var extension = path.Substring(extensionIndex + 1);
                var pathWithoutExtension = path.Substring(0, extensionIndex);
                var layerPath = $"{pathWithoutExtension}_{string.Join("_", layerNames)}.{extension}";

                await using var file = File.OpenWrite(layerPath);
                for (var i = 0; i < pngBytes.Length; i++) {
                    if (i % 4096 == 0) {
                        Progress.Report(progressId, i / (float) pngBytes.Length);
                        await UniTask.Yield();
                    }
                    file.WriteByte(pngBytes[i]);
                }
                await UniTask.Yield();
            } else {
                await using var file = File.OpenWrite(path);

                for (var i = 0; i < pngBytes.Length; i++) {
                    if (i % 4096 == 0) {
                        Progress.Report(progressId, i / (float) pngBytes.Length);
                        await UniTask.Yield();
                    }
                    file.WriteByte(pngBytes[i]);
                }
                await UniTask.Yield();
            }
            toolCamera.ClearTexture();
            await UniTask.Yield();
            Progress.Remove(progressId);
            AssetDatabase.AllowAutoRefresh();

            return null;
        }

        private void IncludeOnlyBounds(Texture2D texture, Bounds bounds) {
            var renderTexture = Data.RenderTexture;
            var pixelsPerUnit = Data.pixelsPerUnit;
            var margin = Data.margin;

            var texturePixelRect = new RectInt(
                0,
                0,
                renderTexture.width,
                renderTexture.height
            );
            var pixelBounds = new RectInt(
                (int) (texturePixelRect.width / 2f - Mathf.Abs((bounds.size.x + margin) * pixelsPerUnit / 2f)),
                (int) (texturePixelRect.height / 2f - Mathf.Abs((bounds.size.y + margin) * pixelsPerUnit / 2f)),
                (int) ((bounds.size.x + margin) * pixelsPerUnit),
                (int) ((bounds.size.y + margin) * pixelsPerUnit)
            );
            var borderRects = new[] {
                new RectInt(0, 0, pixelBounds.xMin, texturePixelRect.height),
                new RectInt(pixelBounds.xMin, pixelBounds.yMax, pixelBounds.width, texturePixelRect.height - pixelBounds.yMax),
                new RectInt(pixelBounds.xMax, 0, texture.width - pixelBounds.xMax, texturePixelRect.height),
                new RectInt(pixelBounds.xMin, 0, pixelBounds.width, pixelBounds.yMin)
            };
            foreach (var borderRect in borderRects) {
                FillRect(texture, borderRect, Color.clear);
            }
        }

        private static void FillRect(Texture2D texture, RectInt texturePixelRect, Color color) {
            var colors = new Color[texturePixelRect.width * texturePixelRect.height];
            for (var i = 0; i < colors.Length; i++) {
                colors[i] = color;
            }
            texture.SetPixels(texturePixelRect.xMin, texturePixelRect.yMin, texturePixelRect.width, texturePixelRect.height, colors);
        }

        #endregion

        #region Shortcut

        [Shortcut("level-capture-tool", KeyCode.C, ShortcutModifiers.Control | ShortcutModifiers.Shift | ShortcutModifiers.Alt,
            displayName = "Level Capture Tool")]
        public static void EnableTool() {
            ToolManager.SetActiveTool<LevelCaptureTool>();
        }

        #endregion

    }
}
