﻿using JetBrains.Annotations;
using UnityEditor;
using UnityEditor.EditorTools;
using UnityEditor.Overlays;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace LR.Tools.LevelCaptureTool.Editor {
    [Overlay(typeof(SceneView), "level-capture-tool-toolbar", "Level Capture Tool"), UsedImplicitly]
    public class LevelCaptureToolOverlay : ToolbarOverlay, ITransientOverlay {

        public bool visible => ToolManager.activeToolType == typeof(LevelCaptureTool);

        protected override Layout supportedLayouts => Layout.All;

        public override VisualElement CreatePanelContent() {
            var uxml = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Packages/com.lr.tools.level-capture-tool/UI/LevelCaptureToolOverlay.uxml");
            var uss = AssetDatabase.LoadAssetAtPath<StyleSheet>("Packages/com.lr.tools.level-capture-tool/UI/LevelCaptureToolOverlay.uss");

            var root = uxml.CloneTree();
            root.styleSheets.Add(uss);

            var levelCaptureTool = LevelCaptureTool.Instance;
            var data = new SerializedObject(levelCaptureTool.Data);

            root.Q<LayerMaskField>("LayerMask").Bind(data);
            root.Q<IntegerField>("PPU").Bind(data);
            root.Q<IntegerField>("PPU").RegisterValueChangedCallback(_ => levelCaptureTool.UpdateCamera());
            root.Q<FloatField>("Margin").Bind(data);
            root.Q<FloatField>("Margin").RegisterValueChangedCallback(_ => levelCaptureTool.UpdateCamera());
            root.Q<Toggle>("SeparateLayers").Bind(data);

            root.Q<Button>("TrimButton").clicked += levelCaptureTool.TrimBounds;
            root.Q<Button>("CaptureButton").clicked += () => levelCaptureTool.Capture().Forget();

            return root;
        }

    }
}
