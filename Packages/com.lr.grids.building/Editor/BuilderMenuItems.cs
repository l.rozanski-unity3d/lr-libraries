﻿using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

#if UNITY_EDITOR

namespace LR.Grids.Building.Editor {
    public static class BuilderMenuItems {

        [MenuItem("Tools/LR/Grids/Create Builder")]
        public static void CreateBuilder() {
            var builder = new GameObject("Builder");

            var ghost = CreateBuildingGhost();
            ghost.transform.SetParent(builder.transform, false);

            var placementGrid = CreatePlacementGrid();
            placementGrid.transform.SetParent(builder.transform, false);
        }

        private static BuildingGhostContainer CreateBuildingGhost() => new GameObject("Building Ghost").AddComponent<BuildingGhostContainer>();

        private static Grid CreatePlacementGrid() {
            var placementGrid = new GameObject("Placement Grid").AddComponent<Grid>();
            var placementTilemap = new GameObject("Tiles", typeof(Tilemap), typeof(TilemapRenderer));
            placementTilemap.transform.SetParent(placementGrid.transform, false);

            return placementGrid;
        }

    }
}

#endif
