﻿using UnityEngine;
using UnityEngine.Tilemaps;

namespace LR.Grids.Building.Extensions {
    public static class BoundsExtensions {

        public static RectInt CellBounds(this Bounds bounds, Tilemap tilemap) {
            var min = tilemap.WorldToCell(bounds.min + (Vector3) Vector2.one * 0.001f);
            var max = tilemap.WorldToCell(bounds.max - (Vector3) Vector2.one * 0.001f);

            return new RectInt(min.x, min.y, max.x - min.x, max.y - min.y);
        }

    }
}
