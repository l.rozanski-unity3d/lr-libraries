﻿#define LR_GRIDS_BUILDING
using System.Collections.Generic;
using UnityEngine;

namespace LR.Grids.Building {
    public class Building : MonoBehaviour {

        public HashSet<BuildingCell> Cells { get; } = new();

        private void Start() {
            Cells.Clear();

            foreach (var cell in GetComponentsInChildren<BuildingCell>()) {
                Cells.Add(cell);
            }
        }

    }
}
