﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace LR.Grids.Building.UI {
    [CreateAssetMenu(menuName = "LR/Grids.Building/Builder Ghost Data")]
    public class BuilderGhostData : ScriptableObject {

        [field: SerializeField]
        public string Label { get; set; }

        [field: SerializeField]
        public Sprite Icon { get; set; }

        [field: SerializeField]
        public Color IconTint { get; set; }

        [field: SerializeField, AssetsOnly]
        public Building BuildingPrefab { get; set; }

        [field: SerializeField, AssetsOnly]
        public BuildingGhost BuildingGhost { get; set; }

    }
}
