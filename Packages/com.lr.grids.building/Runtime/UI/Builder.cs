﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;

namespace LR.Grids.Building.UI {
    public class Builder : MonoBehaviour {

        [SerializeField, SceneObjectsOnly]
        private BuildingGhostContainer ghostContainer;

        [SerializeField, AssetsOnly]
        private VisualTreeAsset buttonTemplate;

        [SerializeField, AssetsOnly]
        private List<BuilderGhostData> buttons;

        [SerializeField, ChildGameObjectsOnly]
        private PlayerInput input;

        private void Start() {
            var document = GetComponent<UIDocument>().rootVisualElement;
            var scrollView = document.Q<ScrollView>("BuilderBar");

            foreach (var ghostData in buttons) {
                var button = buttonTemplate.Instantiate().Q<Button>("BuilderButton");
                button.name = ghostData.name;
                button.clicked += () => Activate(ghostData);

                var icon = button.Q<VisualElement>("Icon");
                icon.style.backgroundColor = ghostData.IconTint;
                icon.style.backgroundImage = new StyleBackground(ghostData.Icon);

                var label = button.Q<Label>("Label");
                label.text = ghostData.Label;
                scrollView.Add(button);
            }
        }

        private void OnEnable() => input.actions["Cancel Placement"].performed += Deactivate;

        private void OnDisable() => input.actions["Cancel Placement"].performed -= Deactivate;

        public void Activate(BuilderGhostData ghostData) {
            ghostContainer.RemoveGhost();

            ghostContainer.Initialize(ghostData.BuildingPrefab, ghostData.BuildingGhost);
            ghostContainer.gameObject.SetActive(true);
        }

        private void Deactivate(InputAction.CallbackContext _) {
            ghostContainer.RemoveGhost();
            ghostContainer.gameObject.SetActive(false);
        }

    }
}
