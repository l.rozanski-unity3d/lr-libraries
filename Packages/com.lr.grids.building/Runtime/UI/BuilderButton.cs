﻿using UnityEngine;

namespace LR.Grids.Building.UI {
    public class BuilderButton : MonoBehaviour {

        [field: SerializeField]
        public Building BuildingPrefab { get; set; }

        [field: SerializeField]
        public BuildingGhost BuildingGhost { get; set; }

    }
}
