﻿using System.Collections.Generic;
using JetBrains.Annotations;
using LR.Core.Utils;
using LR.Grids.Building.Extensions;
using LR.Grids.Building.Placement;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Tilemaps;

namespace LR.Grids.Building {
    [DefaultExecutionOrder(1)]
    public class BuildingGhostContainer : MonoBehaviour {

        private static readonly int WorldPosition = Shader.PropertyToID("_WorldPosition");

        private const int MaxObstacles = 64;
        private static readonly Color ValidPlacementColor = Color.white;
        private static readonly Color InvalidPlacementColor = Color.red;

        [BoxGroup("Prefabs"), UsedImplicitly]
        [field: SerializeField, AssetsOnly, AssetSelector, Required]
        public Building BuildingPrefab { get; set; }

        [BoxGroup("Tilemap Properties"), UsedImplicitly]
        [field: SerializeField, ChildGameObjectsOnly, Required]
        public BuildingGhost Ghost { get; set; }

        [BoxGroup("Tilemap Properties")]
        [SerializeField, SceneObjectsOnly, Required]
        private Tilemap terrainTilemap;

        [BoxGroup("Tilemap Properties")]
        [SerializeField, Required]
        private Grid placementGrid;

        [BoxGroup("Tilemap Properties")]
        [SerializeField, AssetsOnly, Required]
        private TileBase placementTile;

        [BoxGroup("Tilemap Properties")]
        [SerializeField, AssetsOnly, Required]
        private TileBase invalidPlacementTile;

        [BoxGroup("Placement Properties")]
        [SerializeField, PropertyRange(1, 50), DisableInPlayMode]
        private int placementTilemapRadius;

        [SerializeField]
        [BoxGroup("Placement Properties")]
        private LayerMask obstacleLayerMask;

        [SerializeField]
        [BoxGroup("Placement Properties")]
        private List<PlacementRule> additionalRules;

        [SerializeField]
        private PlayerInput input;

        private Tilemap placementTilemap;
        private TilemapRenderer placementTilemapRenderer;
        private Vector3 previousPosition;

        private BuildingCell[] buildingCells;
        private SpriteRenderer[] buildingCellRenderers;
        private RectInt[] buildingCellPlacementRects;

        private readonly Collider2D[] obstacles = new Collider2D[MaxObstacles];
        private Vector3Int[] tilePositions;
        private Color[] tileColors;
        private TileBase[] tiles;
        private bool[,] placementAllowedArray;

        private int Diameter => placementTilemapRadius * 2;

        private Vector3Int CurrentPlacementCell => placementGrid.WorldToCell(Ghost.transform.position);

        private bool canPlace;

        private bool CanPlace {
            get => canPlace;
            set {
                if (canPlace != value) {
                    canPlace = value;
                    SetGhostPlacementColor(canPlace);
                }
            }
        }

        private void SetGhostPlacementColor(bool canPlace) {
            var color = canPlace ? ValidPlacementColor : InvalidPlacementColor;

            foreach (var spriteRenderer in buildingCellRenderers) {
                spriteRenderer.color = color;
            }
        }

        public void Initialize(Building buildingPrefab, BuildingGhost ghost) {
            BuildingPrefab = buildingPrefab;
            Ghost = Instantiate(ghost, transform, false);
            Ghost.Initialize();

            UpdateBuildingCells();
        }

        [UsedImplicitly]
        public void UpdateBuildingCells() {
            buildingCells = Ghost.Cells;
            buildingCellRenderers = Ghost.CellsRenderers;
            buildingCellPlacementRects = new RectInt[buildingCells.Length];
        }

        private void Start() {
            placementTilemap = placementGrid.GetComponentInChildren<Tilemap>(true);
            placementTilemapRenderer = placementGrid.GetComponentInChildren<TilemapRenderer>(true);

            tilePositions = new Vector3Int[Diameter * Diameter];
            tileColors = new Color[Diameter * Diameter];
            tiles = new TileBase[tilePositions.Length];
            placementAllowedArray = new bool[Diameter, Diameter];
        }

        private void OnEnable() => input.actions["Place Building"].performed += PlaceBuilding;
        private void OnDisable() => input.actions["Place Building"].performed -= PlaceBuilding;

        private void Update() {
            var mousePosition = MouseUtils.WorldPosition;
            var position = GridUtils.SnapToGrid(mousePosition, placementGrid.cellSize, false);
            Ghost.transform.position = position;

            if (HasPositionChanged(position)) {
                previousPosition = position;
                RefreshPlacementTilemap();
            }
            if (input.actions["Scroll"].WasPerformedThisFrame()) {
                const float scrollWheelThreshold = 0.1f;
                var scrollWheelDelta = input.actions["Scroll"].ReadValue<float>();

                if (scrollWheelDelta < -scrollWheelThreshold || input.actions["Rotate Building Right"].WasPerformedThisFrame()) {
                    Ghost.CellContainer.transform.RotateAround(Ghost.Pivot.position, Vector3.forward, -90);
                    RefreshPlacementTilemap();
                } else if (scrollWheelDelta > scrollWheelThreshold || input.actions["Rotate Building Left"].WasPerformedThisFrame()) {
                    Ghost.CellContainer.transform.RotateAround(Ghost.Pivot.position, Vector3.forward, 90);
                    RefreshPlacementTilemap();
                }
            }
        }

        private bool HasPositionChanged(Vector3 position) {
            return Mathf.Abs(position.x - previousPosition.x) >= placementGrid.cellSize.x
                || Mathf.Abs(position.y - previousPosition.y) >= placementGrid.cellSize.y;
        }

        private void RefreshPlacementTilemap() {
            placementGrid.transform.position = Ghost.transform.position;
            placementTilemapRenderer.material.SetVector(WorldPosition, Ghost.transform.position);

            var diameter = Diameter;

            Physics2D.SyncTransforms();
            placementTilemap.ClearAllTiles();
            FillPlacementTilemap();

            var arrayStartCellPosition = -new Vector3Int(placementTilemapRadius, placementTilemapRadius);
            PaintObstacleCells(diameter, arrayStartCellPosition);
            PaintTerrainTiles(diameter, arrayStartCellPosition);

            UpdateBuildingCellPlacementRects(arrayStartCellPosition);
            CheckAdditionalRules();

            for (var i = 0; i < tilePositions.Length; i++) {
                var coords = GridUtils.IndexTo2DCoords(i, diameter);

                tiles[i] = placementAllowedArray[coords.x, coords.y] ? placementTile : invalidPlacementTile;
            }
            placementTilemap.SetTiles(tilePositions, tiles);

            for (var i = 0; i < tilePositions.Length; i++) {
                var position = tilePositions[i];
                var color = tileColors[i];

                placementTilemap.SetTileFlags(position, TileFlags.None);
                placementTilemap.SetColor(position, color);
            }
        }

        private void FillPlacementTilemap() {
            var diameter = Diameter;
            var radius = diameter / 2f;
            var center = new Vector2(-placementTilemapRadius + radius, -placementTilemapRadius + radius);

            for (var x = 0; x < diameter; x++) {
                for (var y = 0; y < diameter; y++) {
                    var index = GridUtils.Coords2DToIndex(x, y, diameter);

                    tiles[index] = placementTile;
                    tilePositions[index] = new Vector3Int(-placementTilemapRadius + x, -placementTilemapRadius + y);

                    var tileDistance = Mathf.FloorToInt(((Vector2Int) tilePositions[index] - center).magnitude) / radius;
                    tileColors[index] = new Color(1f, 1f, 1f, 1f - tileDistance);

                    placementAllowedArray[x, y] = true;
                }
            }
        }

        private void PaintObstacleCells(int diameter, Vector3Int arrayStartCellPosition) {
            var placementWorldBounds = GetPlacementWorldBounds(diameter);
            var obstacleCount = Physics2D.OverlapBoxNonAlloc(placementWorldBounds.center, placementWorldBounds.size, 0f, obstacles, obstacleLayerMask);
            var radius = Diameter / 2f;
            var center = new Vector2(-placementTilemapRadius + radius, -placementTilemapRadius + radius);

            for (var i = 0; i < obstacleCount; i++) {
                var placementCellBounds = obstacles[i].bounds.CellBounds(placementTilemap);

                for (var x = placementCellBounds.xMin; x <= placementCellBounds.xMax; x++) {
                    for (var y = placementCellBounds.yMin; y <= placementCellBounds.yMax; y++) {
                        if (x < arrayStartCellPosition.x || x >= arrayStartCellPosition.x + diameter
                            || y < arrayStartCellPosition.y || y >= arrayStartCellPosition.y + diameter) {
                            continue;
                        }
                        var coords = new Vector3Int(x - arrayStartCellPosition.x, y - arrayStartCellPosition.y);
                        var index = GridUtils.Coords2DToIndex(coords.x, coords.y, diameter);

                        tiles[index] = invalidPlacementTile;
                        placementAllowedArray[coords.x, coords.y] = false;

                        var tileDistance = Mathf.FloorToInt(((Vector2Int) tilePositions[index] - center).magnitude) / radius;
                        tileColors[index] = new Color(1f, 0f, 0f, 1f - tileDistance);
                    }
                }
            }
        }

        private Bounds GetPlacementWorldBounds(float diameter) {
            var cellSize = placementTilemap.layoutGrid.cellSize;

            return new Bounds(
                Ghost.transform.position,
                new Vector2(
                    diameter * cellSize.x,
                    diameter * cellSize.y
                )
            );
        }

        private void PaintTerrainTiles(int diameter, Vector3Int arrayStartCellPosition) {
            var radius = Diameter / 2f;
            var center = new Vector2(-placementTilemapRadius + radius, -placementTilemapRadius + radius);

            for (var x = 0; x < diameter; x++) {
                for (var y = 0; y < diameter; y++) {
                    var placementCell = new Vector3Int(arrayStartCellPosition.x + x, arrayStartCellPosition.y + y, 0);
                    var terrainCell = terrainTilemap.WorldToCell(placementTilemap.CellToWorld(placementCell));

                    if (terrainTilemap.HasTile(terrainCell)) {
                        continue;
                    }
                    var arrayCoords = new Vector3Int(x, y);
                    var arrayIndex = GridUtils.Coords2DToIndex(arrayCoords.x, arrayCoords.y, diameter);

                    tiles[arrayIndex] = invalidPlacementTile;
                    placementAllowedArray[x, y] = false;

                    var index = GridUtils.Coords2DToIndex(x, y, Diameter);
                    var tileDistance = Mathf.FloorToInt(((Vector2Int) tilePositions[index] - center).magnitude) / radius;
                    tileColors[index] = new Color(1f, 0f, 0f, 1f - tileDistance);
                }
            }
        }

        private void UpdateBuildingCellPlacementRects(Vector3Int arrayStartCellPosition) {
            var diameter = Diameter;

            for (var i = 0; i < buildingCells.Length; i++) {
                buildingCellPlacementRects[i] = buildingCells[i].Collider.bounds.CellBounds(placementTilemap);

                for (var x = buildingCellPlacementRects[i].xMin; x <= buildingCellPlacementRects[i].xMax; x++) {
                    for (var y = buildingCellPlacementRects[i].yMin; y <= buildingCellPlacementRects[i].yMax; y++) {
                        if (x < arrayStartCellPosition.x || x >= arrayStartCellPosition.x + diameter
                            || y < arrayStartCellPosition.y || y >= arrayStartCellPosition.y + diameter) {
                            continue;
                        }
                        if (placementAllowedArray[x - arrayStartCellPosition.x, y - arrayStartCellPosition.y]) {
                            continue;
                        }
                        CanPlace = false;
                        return;
                    }
                }
            }
            CanPlace = true;
        }

        private void CheckAdditionalRules() {
            foreach (var buildingCell in buildingCells) {
                if (!CanPlace) {
                    break;
                }
                var buildingCellBounds = buildingCell.Collider.bounds.CellBounds(terrainTilemap);

                foreach (var placementRule in additionalRules) {
                    for (var x = buildingCellBounds.xMin; x <= buildingCellBounds.xMax; x++) {
                        for (var y = buildingCellBounds.yMin; y <= buildingCellBounds.yMax; y++) {
                            var terrainCell = new Vector3Int(x, y);

                            if (placementRule.CanPlaceCell(buildingCell, terrainCell, terrainTilemap)) {
                                continue;
                            }
                            CanPlace = false;
                            placementAllowedArray[x, y] = false;
                            break;
                        }
                    }
                }
            }
        }

        private void PlaceBuilding(InputAction.CallbackContext _) {
            if (!CanPlace) {
                return;
            }
            Instantiate(BuildingPrefab, Ghost.CellContainer.transform.position, Ghost.CellContainer.transform.rotation);
            RefreshPlacementTilemap();
        }

        public void RemoveGhost() {
            if (Ghost == null) {
                return;
            }
            Destroy(Ghost.gameObject);

            Ghost = null;
            BuildingPrefab = null;
        }

#if UNITY_EDITOR

        [Button]
        [ShowIf("@placementGrid == null")]
        private void CreatePlacementGrid(float cellSize = 1f, string spriteSortingLayer = "Default") {
            var grid = new GameObject("Placement Grid").AddComponent<Grid>();
            grid.transform.SetParent(transform, false);
            grid.cellSize = new Vector3(cellSize, cellSize, 0);

            var tilemap = new GameObject("Tilemap").AddComponent<Tilemap>();
            tilemap.transform.SetParent(grid.transform, false);

            var tilemapRenderer = tilemap.gameObject.AddComponent<TilemapRenderer>();
            tilemapRenderer.sortingOrder = 1;
            tilemapRenderer.sortingLayerName = spriteSortingLayer;

            placementGrid = grid;
            placementTilemap = tilemap;
            InspectorUtilities.RegisterUnityObjectDirty(this);
        }

#endif

    }
}
