﻿using UnityEngine;

namespace LR.Grids.Building {
    public class BuildingCell : MonoBehaviour {

        [SerializeField]
        private new BoxCollider2D collider;

        public BoxCollider2D Collider => collider;

    }
}
