﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace LR.Grids.Building.Placement {
    [CreateAssetMenu(menuName = "LR/Grids.Building/Placement Rules/" + nameof(TileTypePlacementRule))]
    public class TileTypePlacementRule : PlacementRule {

        [SerializeField]
        private bool allowPlacementOnEmptySpace;

        [SerializeField]
        private List<TileBase> tileTypes;

        public override bool CanPlaceCell(BuildingCell buildingCell, Vector3Int tilemapCell, Tilemap tilemap) {
            var tileBase = tilemap.GetTile(tilemapCell);

            if (tileBase == null) {
                return allowPlacementOnEmptySpace;
            }
            foreach (var tileType in tileTypes) {
                if (tileBase == tileType) {
                    return true;
                }
            }
            return false;
        }

    }
}
