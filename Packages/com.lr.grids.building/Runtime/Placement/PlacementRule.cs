﻿using UnityEngine;
using UnityEngine.Tilemaps;

namespace LR.Grids.Building.Placement {
    public abstract class PlacementRule : ScriptableObject {

        public bool CanPlace(Building building, Tilemap tilemap) {
            foreach (var buildingCell in building.Cells) {
                var cellPosition = tilemap.WorldToCell(buildingCell.transform.position);

                if (!CanPlaceCell(buildingCell, cellPosition, tilemap)) {
                    return false;
                }
            }
            return true;
        }

        public abstract bool CanPlaceCell(BuildingCell buildingCell, Vector3Int tilemapCell, Tilemap tilemap);

    }
}
