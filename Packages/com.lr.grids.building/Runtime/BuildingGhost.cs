﻿using UnityEngine;

namespace LR.Grids.Building {
    [DefaultExecutionOrder(0)]
    public class BuildingGhost : MonoBehaviour {

        [field: SerializeField]
        public Transform Pivot { get; set; }

        [field: SerializeField]
        public Transform CellContainer { get; set; }

        public BuildingCell[] Cells { get; set; }

        public SpriteRenderer[] CellsRenderers { get; set; }

        private void Start() {
            Initialize();
        }

        public void Initialize() {
            Cells = CellContainer.GetComponentsInChildren<BuildingCell>(true);
            CellsRenderers = CellContainer.GetComponentsInChildren<SpriteRenderer>(true);
        }

    }
}
