﻿using System.Collections.Generic;
using LR.Grids.Building.UI;
using UnityEngine;
using UnityEngine.UIElements;

public class UIToolkitTest : MonoBehaviour {

    [SerializeField]
    private Builder builder;

    [SerializeField]
    private VisualTreeAsset buttonTemplate;

    [SerializeField]
    private List<BuilderGhostData> buttons;

    private void Start() {
        var document = GetComponent<UIDocument>().rootVisualElement;

        var scrollView = document.Q<ScrollView>("BuilderBar");
        foreach (var ghostData in buttons) {
            var button = buttonTemplate.Instantiate().Q<Button>("BuilderButton");
            button.name = ghostData.name;
            button.clicked += () => builder.Activate(ghostData);

            var label = button.Q<Label>("Label");
            label.text = ghostData.Label;
            scrollView.Add(button);
        }
    }

}
