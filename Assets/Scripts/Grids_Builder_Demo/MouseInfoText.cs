﻿using System;
using LR.Core.Utils;
using TMPro;
using UnityEngine;

namespace Grids_Builder_Demo {
    public class MouseInfoText : MonoBehaviour {

        [SerializeField]
        private TextMeshProUGUI text;

        [SerializeField]
        private Grid placementGrid;

        [SerializeField]
        private Transform ghost;
        
        private void Update() {
            var mousePosition = MouseUtils.WorldPosition;
            var gridPosition = GridUtils.SnapToGrid(mousePosition, placementGrid.cellSize, false);
            var gridPositionRounded = GridUtils.SnapToGrid(mousePosition, placementGrid.cellSize, true);
            var ghostPosition = ghost.position;

            text.text = $@"
Position: {mousePosition}
Grid Position: {gridPosition}
Grid Rounded: {gridPositionRounded}
Ghost Position: {ghostPosition}
            ".Trim();
        }

    }
}
