using UnityEditor;
using UnityEditor.Toolbars;
using UnityEngine;

namespace Editor {
    [EditorToolbarElement(ID, typeof(SceneView))]
    public class TestEditorToolbarButton : EditorToolbarButton {

        public const string ID = "TestEditorToolbar/Buton";

        public TestEditorToolbarButton() {
            text = "Test Button";
            icon = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Sprites/Icon.png");
            tooltip = "Tests the button";
            clicked += OnClick;
        }

        private static void OnClick() {
            Debug.Log("Clicked TestEditorToolbarButton!");
        }

    }
}
