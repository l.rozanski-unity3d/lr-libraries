using UnityEditor;
using UnityEditor.Overlays;
using UnityEngine;
using UnityEngine.UIElements;

namespace Editor {
    [Overlay(typeof(SceneView), "Test Editor Tool", true)]
    public class TestEditorTool : Overlay {

        public override VisualElement CreatePanelContent() {
            var content = new VisualElement();
            content.style.flexDirection = FlexDirection.Column;
            content.Add(new Label("Test 1"));
            content.Add(new Label("Test 2"));
            content.Add(new Label("Test 3"));

            var button = new Button(() => Debug.Log("Clicked!"));
            button.text = "Click me!";

            content.Add(button);

            return content;
        }

    }
}
