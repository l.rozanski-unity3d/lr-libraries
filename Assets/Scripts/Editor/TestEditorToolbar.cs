using UnityEditor;
using UnityEditor.Overlays;

namespace Editor {
    [Overlay(typeof(SceneView), "Test Editor Toolbar", true)]
    public class TestEditorToolbar : ToolbarOverlay {

        protected override Layout supportedLayouts => Layout.All;

        public TestEditorToolbar() : base(
            TestEditorToolbarButton.ID
        ) { }

    }
}
