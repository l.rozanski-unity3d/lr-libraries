﻿using LR.FSM;
using Sirenix.OdinInspector;
using UnityEngine;

namespace FSM_Demo {
    public class WalkingState : State<Entity> {

        [ShowInInspector, ReadOnly]
        private Vector3 target;

        [SerializeField, PropertyRange(0f, 100f)]
        private float speed;

        public override void OnEnter(Entity instance) {
            target = instance.transform.position + (Vector3)Random.insideUnitCircle;
        }

        public override void OnUpdate(Entity instance) {
            var position = instance.transform.position;

            if (Vector3.Distance(position, target) < 0.1f) {
                StateMachine.ActivateState("Idle");
                return;
            }
            instance.transform.position += (target - position).normalized * (speed * Time.deltaTime);
        }

        public override void OnExit(Entity instance) {
            target = instance.transform.position;
        }
    }
}
