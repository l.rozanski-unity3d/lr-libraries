﻿using LR.FSM;
using UnityEngine;

namespace FSM_Demo {
    public class Entity : MonoBehaviour {

        [SerializeField]
        private StateMachine<Entity> stateMachine;

        private void Start() {
            stateMachine.Init(this);
            stateMachine.ActivateState("Idle");
        }

        private void Update() {
            stateMachine.UpdateState();
        }

    }
}
