﻿using Cysharp.Threading.Tasks;
using LR.FSM;

namespace FSM_Demo {
    public class IdleState : State<Entity> {

        public override async void OnEnter(Entity instance) {
            await Init();
        }

        private async UniTask Init() {
            await UniTask.Delay(1000);
            StateMachine.ActivateState("Walking");
        }

        public override void OnUpdate(Entity instance) { }

        public override void OnExit(Entity instance) { }
    }
}
