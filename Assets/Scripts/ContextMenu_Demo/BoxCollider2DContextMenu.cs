﻿using LR.UI.Menu;
using TheraBytes.BetterUi;
using UnityEngine;

namespace ContextMenu_Demo {
    public class BoxCollider2DContextMenu : AbstractContextMenu<BoxCollider2D> {

        [SerializeField]
        private BetterTextMeshProUGUI description;
        
        public override void OnOpen() {
            description.text = $"BoxCollider2D size: {Context.size}";
            
            Debug.Log($"OnOpen with context: {Context}");
        }

        public override void OnClose() {
            Debug.Log($"OnClose with context: {Context}");
        }

    }
}
